﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ScrabbleDecoder
{
    class Program
    {

        static void Main(string[] args)
        {
            bool isPlaying = true;
            string[] words = File.ReadAllLines("words2.txt");
            Console.WriteLine("Type the letters you want checked.");
            while (isPlaying)
            {
                string letters = Console.ReadLine().ToUpper();
                Console.WriteLine();
                List<string> possible = new List<string>();
                Dictionary<string, int> wordScore = new Dictionary<string, int>();
                foreach (var word in words)
                {
                    if (containsLetters(letters, word))
                    {
                        possible.Add(word);
                    }
                }
                foreach(var p in possible)
                {
                    int points = PointCalculator(p);
                    wordScore.Add(p, points);
                }
                var items = from pair in wordScore orderby pair.Value ascending select pair;
                foreach (var p in items)
                {
                    Console.WriteLine("{0} : {1}", p.Key, p.Value);
                }
                Console.WriteLine();
            }
        }

        private static Boolean containsLetters(string input, string word)
        {
            for(int i = 0; i < word.Length; i++)
            {
                if (!input.Contains(word[i]))
                {
                    return false;
                }
                if (input.Contains(word[i]))
                {
                    input = input.Remove(input.IndexOf(word[i]), 1);
                    word = word.Remove(i,1);
                    i--;
                }
            }
           
            return true;
        }

        private static int PointCalculator(string word)
        {
            int sum = 0;
            foreach(var c in word)
            {
                sum += Points(c);
            }
            return sum;
            
        }

        private static int Points(char c)
        {
            switch (c)
            {
                case 'E': case 'A': case 'O': case 'T': case 'I': case 'N': case 'R': case 'S': case 'L': case 'U':
                    return 1;
                case 'D': case 'G':
                    return 2;
                case 'B': case 'C': case 'M': case 'P':
                    return 3;
                case 'F': case 'H': case 'V': case 'W': case 'Y':
                    return 4;
                case 'K':
                    return 5;
                case 'J': case 'X':
                    return 8;
                case 'Q': case 'Z':
                    return 10;
            }
            return 0;
        }
    }


}
